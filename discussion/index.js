console.log("Hello World!");

// There are two types of comments:

   /* 
   1. The single-line comment denoted by two slashes
    2.The multi-line comment denoted by a slash and asterisk
    */

// [SECTION] Syontax and Statements

		// Statements in programming are instructions that we tell the computer to perform
		// JS statements usually end with semicolon (;)
		// Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends
		// A syntax in programming, it is the set of rules that describes how statements must be constructed
		// All lines/blocks of code should be written in a specific manner to work. This is due to how these codes were initially programmed to function and perform in a
		// certain manner

// [SECTION] Variables

		// Any information that is used by an application is stored in what we call a "memory"
		// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
		// This makes it easier for us associate information stored in our devices to actual "names" about information

// Declaring Variables - tells our devices that a variable name is created and is ready to store data
/*
	Syntax: let/const variableName;
*/

// let myVariable;
// console.log (myVariable);

// console.log(hello);

// let hello;
// console.log(hello);

/*
	Guide in writingd variables:
		1. Use the 'let' keyword followed by the variable name of your choice and use the assignment operator (=) to assign value.
		2. Variable names should start with a lowercase letter, use camelCase for multiple words.
			ex: firstWord, myCamelCase, thisIsAnExample
		3. For constant variables, use the 'const' keyword.
		4. Variables names should be indicative (or descriptive) of the value being stored to avoid confusion

	Best Practices in writing variables:

		1. When naming variables, it is important to create variables that are discriptive and indicative of the data it contains

			let firstName = "Michael"; -- good
			let pokemon = 250000; --bad

		2. When naming variables, it is better to start with a lowercas letter. We usually avoid creating variable names that starts with capitald letters. Because there are several keywords is JS that start in capital letter.

		3. Do not add spaces to your variable names. Use camelCase for multiple words or underscores.

			let first name -- bad
			let firstName -- good
			let first_name --good
			
*/

let productName = 'Desktop Computer';
console.log(productName);

// Reassigning Variable Values
// changing the initial or previous value into another new value
/*
	Syntax:
		variableName = new_value;
*/

productName = 'Laptop';
console.log(productName);


// let variable cannot be re-declared within its scope.
let friend = 'Kate';
friend = 'Jane';
// // But we cannot re-delared the variable as it will return an error of "friends has already been declared"
// let friends = 'Kate';
// let friends = 'Jane';



// values of constant cannot be change and will simply return an error
// interest = 6;
// console.log(interest);

// Reassigning Variables vs Initializing Variables
// Declare a Variable
let supplier;

// Initialization is done after the variable has been declared
supplier = "John Smith Trading"
console.log(supplier);

// Reassigning is done after a variable is changed from a previous value
supplier = "Jane Smith Trading"
console.log(supplier);

// const variable cannot be declared without initialization 
// const pi = 3.14;
// // pi = 3.14;
// console.log(pi);

// var vs let/const
// var - is also used in declaring a variable, but is its developed in ECMAScript1 (1997) and let/const is introduced in ES6(2015)

// we practice using let/const to avoid hoisting.
// Hoisting is JavaScript's default behaviour of moving declarations to the top.

a = 5;
console.log(a);
var a;

// b = 5;
// console.log(b);
// let b;

// let/const local/global scope
// scope is essentially where the variables are available for use

// global scope
let outerVariable = 'hello';

{
	// local scope
	let innerVariable = 'its me again';
}

console.log(outerVariable);
// console.log(innerVariable); -- innerVariable is not defined

const outerVariable2 = 'hi';

{
	const innerVariable2 = "i'm back";
}

console.log(outerVariable2);
// console.log(innerVariable2); -- innerVariable2 is not defined

// Multiple Variable declarations
// when you declare variable in one line
// we can only combine variables with the same keyword

// let productCode = 'DC107', productBrand = 'Dell';
let productCode = 'DC107';
const productBrand = 'Dell';
console.log(productCode,productBrand);

// [SECTION] Data types

// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text// Strings in JavaScript can be written using either a single (') or double (") quote
// In other programming languages, only the double quotes can be used for creating strings

let country = 'Philippines';
let province = 'Metro Manila';

// Concatenating Strings
// Multiple string values can be combined to create a single string using the "+" symbol

let address = province + ',' + country;
console.log(address); // Metro Manila, Philippines

let greeting = 'Mabuhay' + country;
console.log(greeting);

// escape character (\n)
	// creates a new line
let mailAddress = "Metro Manila \n\n Philippines";
console.log(mailAddress);

/*
	Metro Manila

	Philippines
*/

// Using the double quotes along with the single quotes can allow us to easily include single quotes in texts without using the escape character

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/Whole numbers

let headCount = 26;
console.log(headCount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let distance = 2e10;
console.log(distance);

// Combine text and numbers
let message3 = "John's grade last quarter is " + grade;
console.log(message3);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussions about creating logic to make our application respond to certain scenarios

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types
/*
	Syntax:
		let/const arrayName = [elementA, elementB, ...., elementN]
*/

let grades = [98.7, 92.1, 90.2, 94.0];
console.log(grades);

let random = ["John", "Smith", 32, true];
console.log(random);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items
// They're used to create complex data that contains pieces of information that are relevant to each other
// Every individual piece of information is called a property of the object

/*
	Syntax:
		let/const objectName = {
			propertyA: value,
			propertyB: value,...
		}
*/
let person = {
	firstName: "John",
	lastName: "Smith",
	age: 32,
	isMarried: true
}
console.log(person);

// typeof operator is used to determine the type of data or the value of a variable. It outputs a string.
console.log(typeof person);

// Array is a special type of object with methods and functions to manipulate it.
console.log(typeof grades);
console.log(typeof country);

/*
	Constant Objects and Arrays
		The keyword const is a little misleading.

		It does not define a constant value. It defines a constant reference to a value.

		Because of this you can NOT:

		Reassign a constant value
		Reassign a constant array
		Reassign a constant object

		But you CAN:

		Change the elements of constant array
		Change the properties of constant object
*/

const anime = ['one piece', 'one punch man', 'attack on titan'];
// anime = ['kimetsu no yaiba'];

console.log(anime);

// const anime = ['one piece', 'one punch man', 'attack on titan'];
// anime[0] = ['kimetsu no yaiba'];

// console.log(anime0);

anime[1] = 'kimetsu no yaiba'
console.log(anime);

//We can change the element of an array assigned to a constant variable.
//We can also change the object's properties assigned to a constant variable.

// Null
// It is used to intentionally express the absence of a value in a variable declaration/initialization
		// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified
		let spouse = null;
		// Using null compared to a 0 value and an empty string is much better for readability purposes

		// null is also considered as a data type of it's own compared to 0 which is a data type of a number and single quotes which are a data type of a string
		let myNumber = 0;
		let myString = '';

		// Undefined
		// Represents the state of a variable that has been declared but without an assigned value
		let fullName;
		console.log(fullName);
// Undefined vs Null
		// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
		// null means that a variable was created and was assigned a value that does not hold any value/amount
		// Certain processes in programming would often return a "null" value when certain tasks results to nothing
		let varA = null;
		console.log(varA);
// For undefined, this is normally caused by developers creating variables that have no value/data associated with them
		// This is when the value of a variable is still unknown
		let varB;
		console.log(varB);